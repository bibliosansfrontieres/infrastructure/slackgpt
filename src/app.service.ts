import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import OpenAI from 'openai';
import { SlackCommandDto } from './dto/slack-command.dto';
import axios from 'axios';

@Injectable()
export class AppService {
  private openAi: OpenAI;
  private prompts: Map<string, string>;

  constructor(configService: ConfigService) {
    this.openAi = new OpenAI({
      apiKey: configService.get('OPEN_AI_API_KEY'),
    });
    this.loadPrompts();
  }

  /**
   * Loads the prompts from the environment variables and sets them in the prompts property.
   *
   * @private
   * @return {void}
   */
  private loadPrompts(): void {
    const prompts = new Map();

    for (const key in process.env) {
      if (key.startsWith('PROMPT_')) {
        prompts.set(key.replace('PROMPT_', ''), process.env[key]);
      }
    }

    this.prompts = prompts;
  }

  /**
   * Retrieves the text of a prompt.
   *
   * @param {string} prompt - The prompt identifier.
   * @return {string} The text of the prompt.
   */
  private getPromptText(prompt: string): string {
    const promptText = this.prompts.get(prompt);

    if (promptText === undefined) {
      throw new NotFoundException(`prompt "${prompt}" not found`);
    }

    return promptText;
  }

  /**
   * Asynchronously posts a question and returns the answer.
   *
   * @param {string} prompt - The prompt for the question.
   * @param {string} question - The question to be posted.
   * @return {Promise<string>} The answer to the question.
   */
  async postQuestion(
    prompt: string,
    slackCommandDto: SlackCommandDto,
  ): Promise<any> {
    const question = slackCommandDto.text;

    if (!question) {
      return {
        text: 'La question ne peut être vide ! / Question cannot be empty !',
        reponse_type: 'ephemeral',
      };
    }

    if (question.length < 10) {
      return {
        text: 'La question doît faire au moins 10 caractères ! / Question must be at least 10 characters !',
        reponse_type: 'ephemeral',
      };
    }

    if (question.split(' ').length < 3) {
      return {
        text: 'La question doit contenir au moins 3 mots ! / Question must contain at least 3 words !',
        reponse_type: 'ephemeral',
      };
    }

    this.askQuestion(prompt, slackCommandDto);

    return {
      text: 'Laisse moi réfléchir un peu... / Let me think a little...',
      reponse_type: 'ephemeral',
    };
  }

  async askQuestion(prompt: string, slackCommandDto: SlackCommandDto) {
    const question = slackCommandDto.text;
    const promptText = this.getPromptText(prompt);
    const completion = await this.openAi.chat.completions.create({
      messages: [
        {
          role: 'system',
          content: `${promptText}${question}`,
        },
      ],
      model: 'gpt-3.5-turbo',
    });

    const answer = completion.choices[0].message.content;

    const text = `
*Question reçue / Question received :*\n\n
<@${slackCommandDto.user_id}> : ${question}\n\n
*Réponse obtenue / Answer obtained :*\n\n
${answer}\n\n
----------
\n\n
*Si cette réponse n'est pas satisfaisante, merci d'envoyer un message dans <#CFWKYCMPW|dnum>, la DNUM est là pour ça.*\n
*If this answer is not satisfactory, please send a message in <#CFWKYCMPW|dnum>, the DNUM is waiting for it.*
`;

    await axios.post(
      slackCommandDto.response_url,
      { text, response_type: 'in_channel' },
      { headers: { 'Content-Type': 'application/json' } },
    );
  }
}
