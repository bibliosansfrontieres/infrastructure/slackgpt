import * as Joi from 'joi';

export const configurationSchema = Joi.object({
  OPEN_AI_API_KEY: Joi.string(),
});
