import { Body, Controller, HttpCode, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { SlackCommandDto } from './dto/slack-command.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post(':prompt')
  @HttpCode(200)
  postQuestion(
    @Param('prompt') prompt: string,
    @Body() slackCommandDto: SlackCommandDto,
  ) {
    return this.appService.postQuestion(prompt, slackCommandDto);
  }
}
